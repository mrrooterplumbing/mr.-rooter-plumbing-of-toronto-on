Every customer's individual needs is important to us at Mr. Rooter. Our focus is to provide a quality combination of plumbing and drain service and products. We service locations near you:
Toronto, North York, Scarborough, Brampton, York Region, & Durham Region.

Address: 27 Glenmount Park Rd, Toronto, ON M4E 2M8 || Phone: 416-699-8623
